// import thư viện express
const express = require("express"); // Tương tự : import express from "express";

// import thư viện path
const path = require("path");

// Khởi tạo Express App
const app = express();

// Khai báo để sử dụng UTF-8
app.use(express.urlencoded({
    extended: true
}));

// Khai báo thư viện Mongoose
const mongoose = require("mongoose");

// Cấu hình request đọc được body json
app.use(express.json());

// Khai báo router app
const courseRouter = require("./app/routes/courseRouter");

//Khai báo port sử dụng
const port = 8000;

// Khai báo APi dạng Get "/" sẽ chạy vào đây
app.get("/", (request, response) => {
    console.log(__dirname);
    //Chạy file HTML với đường dẫn / cần dòng 2
    response.sendFile(path.join(__dirname + "/views/course365/index.html"));

})

// Khai báo APi dạng Get "/" sẽ chạy vào đây
app.get("/CourseDetail", (request, response) => {
    console.log(__dirname);
    //Chạy file HTML với đường dẫn / cần dòng 2
    response.sendFile(path.join(__dirname + "/views/course365/CourseDetail.html"));

})

// Kết nối với MongoDB:
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Course_365", (error) => {
    if (error) throw error;
    console.log("Connect MongoDB successfully!");
})

// App sử dụng router
app.use("/", courseRouter);


//Để hiển thị ảnh cần thêm middleware static vào express
app.use(express.static(__dirname + "/views/course365"))


app.listen(port, () => {
    console.log(`App Listening on port ${port}`);
})