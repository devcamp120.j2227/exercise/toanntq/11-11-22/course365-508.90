// Khai báo thư viện ExpressJS
const express = require("express");

// Khai báo router app
const router = express.Router();

// Import course middleware
const {
    getAllCourseMiddleware,
    createCourseMiddleware,
    getDetailCourseMiddleware,
    updateCourseMiddleware,
    deleteCourseMiddleware
} = require("../middlewares/courseMiddleware");

// Import course controller
const {
    getAllCourse,
    createCourse,
    getCourseById,
    updateCoureById,
    deleteCourseById
} = require("../controllers/courseController")

router.get("/courses", getAllCourseMiddleware, getAllCourse);

router.post("/courses", createCourseMiddleware, createCourse);

router.get("/courses/:courseId", getDetailCourseMiddleware, getCourseById)

router.put("/courses/:courseId", updateCourseMiddleware, updateCoureById)

router.delete("/courses/:courseId", deleteCourseMiddleware, deleteCourseById)

module.exports = router;