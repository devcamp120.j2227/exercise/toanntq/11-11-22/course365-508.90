//Hàm thực hiện tải trang Index 
function onPageLoading() {
    debugger
    loadCourse()
}

//Load các khóa học 
function loadCourse() {
    //Khởi tạo Obj
    var vPopularCourseArr = [];
    var vTrendingCourseArr = [];
    //Get Data
    getCourseWithCondition(vPopularCourseArr, vTrendingCourseArr);

    //Display DOM
    loadPopularCourseToWeb(vPopularCourseArr);
    loadTrendingCourseToWeb(vTrendingCourseArr);
}

//Load các khóa học trending
function loadTrendingCourseToWeb(paramPopCourse) {
    //Tạo chuỗi các thẻ card từ mảng
    for (let index = 0; index < paramPopCourse.length; index++) {
        var vCourseIndex = paramPopCourse[index]
            //Khởi tạo biến chứa html
        var vElementString = '';
        vElementString = `<div class="card" style="width: 16rem;">
                                <img class="card-img-top" src=` + vCourseIndex.coverImage + ` alt="Card image cap">
                                <div class="card-body">
                                    <h6 class="card-title text-primary">` + vCourseIndex.courseName + `</h6>
                                    <div class="container row form-inline">
                                        <i class="fa fa-regular fa-clock "></i>&nbsp;
                                        <label class="form-group" for="">` + vCourseIndex.duration + `</label>&nbsp;
                                        <label class="text-dark form-group">` + vCourseIndex.level + `</label>
                                    </div>
                                    <div class="container row form-inline mt-2">
                                        <label class="font-weight-bold" for="">$` + vCourseIndex.discountPrice + `</label>&nbsp;
                                        <label for=""><del>$` + vCourseIndex.price + `</del></label>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="container-fluid row">
                                        <div class="form-group mt-2 col-sm-11 p-0">
                                            <img class="img-fluid rounded-circle" width="35" src=` + vCourseIndex.teacherPhoto + `>
                                            <label for="">` + vCourseIndex.teacherName + `</label>
                                        </div>
                                        <div class="mt-2 mr-0 pr-0 text-right">
                                            <i class="fa-regular fa-bookmark"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>`;
        //Thêm chuỗi html này
        $('.trending-card-container').append(vElementString)
    }
}

//Load các khóa học Popular
function loadPopularCourseToWeb(paramPopCourse) {
    //Tạo chuỗi các thẻ card từ mảng
    for (let index = 0; index < paramPopCourse.length; index++) {
        var vCourseIndex = paramPopCourse[index]
            //Khởi tạo biến chứa html
        var vElementString = '';
        vElementString = `<div class="card" style="width: 16rem;">
                                <img class="card-img-top" src=` + vCourseIndex.coverImage + ` alt="Card image cap">
                                <div class="card-body">
                                    <h6 class="card-title text-primary">` + vCourseIndex.courseName + `</h6>
                                    <div class="container row form-inline">
                                        <i class="fa fa-regular fa-clock "></i>&nbsp;
                                        <label class="form-group" for="">` + vCourseIndex.duration + `</label>&nbsp;
                                        <label class="text-dark form-group">` + vCourseIndex.level + `</label>
                                    </div>
                                    <div class="container row form-inline mt-2">
                                        <label class="font-weight-bold" for="">$` + vCourseIndex.discountPrice + `</label>&nbsp;
                                        <label for=""><del>$` + vCourseIndex.price + `</del></label>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="container-fluid row">
                                        <div class="form-group mt-2 col-sm-11 p-0">
                                            <img class="img-fluid rounded-circle" width="35" src=` + vCourseIndex.teacherPhoto + `>
                                            <label for="">` + vCourseIndex.teacherName + `</label>
                                        </div>
                                        <div class="mt-2 mr-0 pr-0 text-right">
                                            <i class="fa-regular fa-bookmark"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>`;
        //Thêm chuỗi html này
        $('.container-popular-card').append(vElementString)
    }
}

//Hàm thu tập mảng các khóa học theo điều kiện 
function getCourseWithCondition(paramPopCourse, paramTrendCourse) {
    for (let index = 0; index < gCoursesDB.courses.length; index++) {
        if (gCoursesDB.courses[index].isPopular == true) {
            paramPopCourse.push(gCoursesDB.courses[index])
        }
        if (gCoursesDB.courses[index].isTrending == true) {
            paramTrendCourse.push(gCoursesDB.courses[index])
        }
    }
    console.log({ paramPopCourse, paramTrendCourse })
}